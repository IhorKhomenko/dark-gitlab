#+STARTUP: nofold

* Changelog
** 1.6.3 [2020-11-05 Thu]
Small patch release to fix a few things for jobs in pipelines, and font used in
GFM commits and commit ranges.

*** Fixed
- Updated styles for text truncating of jobs names in pipelines ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/47][!47]])
  + Also added a workaround for glitchy behavior in [[https://gitlab.com/vednoc/dark-gitlab/-/commit/6f66369aeb1b6f6167766a3bbd0016d14c643a60][#6f66369a]].
  + Might be bundled into a "compact pipelines" option in next minor release.
- GFM commits and commit ranges now use custom monospace font

** 1.6.2 [2020-11-03 Tue]
Small patch release to fix a few unstyled elements and a variable conflict.

*** Fixed
- Added styles for applications in Kubernetes clusters ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/142][#142]])
- Updated styles for hover event for build jobs in pipelines
- Renamed =v= variable for =bpoints= to escape a conflict with =v= mixin

** 1.6.1 [2020-10-27 Tue]
Small patch release to fix styles for a couple of unstyled areas.

*** Fixed
- Added a new selector for titles of cards ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/140][#140]])
- Added a temporary fix for 'DevOps Score' page ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/141][#141]])
- Remapped another obscure and possibly obsolete color utility class

** 1.6.0 [2020-10-25 Sun]
Adding support for =(prefers-color-scheme: dark)= and refactoring old CSS literals
for inputs and up/down arrows to Stylus-lang.

*** Added
- A new option to interop with =(prefers-color-scheme: dark)= media query ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/87][#87]])
  + Disabled by default, and only available in the ~wa.user.styl~ version.
  + It will turn dark mode on or off depending on your system's theme.

*** Improved
- Added vertical whitespace between multiple rows of badges in project header
- Refactored logic for radio/checkbox inputs and ↑/↓ arrows to Stylus-lang

** 1.5.8 [2020-10-24 Sat]
Adding new domains and tweaking missing/static buttons.

*** Added
- Lots of new domains ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/45][!45]], [[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/46][!46]])

*** Improved
- Switched to darker text color for missing/static buttons

** 1.5.7 [2020-10-23 Fri]
Updating styles to fix changes introduced in GitLab =13.5= release.

*** Fixed
- Inverted default GitHub logo on sign in page ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/138][#138]], [[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/44][!44]])
- Added new colors for milestone/prioritized labels ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/139][#139]], [[https://gitlab.com/vednoc/dark-gitlab/-/issues/137][#137]])
- Added styles for 'Authorize an application' modal in user settings
- Added styles for 'Requirements' page and fixed bugs in that area

*** Improved
- Increased the consistency of colors for inputs in settings area
- Remapped more colors from utility classes

** 1.5.6 [2020-10-15 Thu]
Minor release to fix a couple of bugs.

*** Fixed
- Added styles for Kubernetes cluster list ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/43][!43]])
- Removed border/background from transparent buttons in file diff titles

** 1.5.5 [2020-10-14 Wed]
Fixing a few unstyled areas in issues and boards, as well as recent additions to
lables in issues.

*** Improved
- Tweaked dark overlay and added styles for close buttons to labels

*** Fixed
- Added styles for 'Manual' sorting mode in issues ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/136][#136]])
- Added styles for 'Add issues' modal in Issue Boards

** 1.5.4 [2020-10-10 Sat]
Fixing a regression introduced in the previous update.

*** Fixed
- Removed an extra selector for toolbar buttons in file viewer

** 1.5.3 [2020-10-08 Thu]
Fixing recently introduced changes for the upcoming =13.5= release.

*** Fixed
- Added reset styles for Bootstrap stacked tables ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/41][!41]])
- Added resets for new buttons introduced in Pajamas
- Added tweaks for new badge styles in Issue Boards

** 1.5.2 [2020-10-05 Mon]
Small patch release to fix things in Merge Requests.

*** Fixed
- Added colors for CI widget and new CI status icons ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/135][#135]])
- Added styles for deployment and linked downstream pipeline

** 1.5.1 [2020-10-01 Thu]
Small patch release to fix things in pipelines and jobs.

*** Fixed
- Updated specificity for jobs's full text tweaks in pipelines
- Updated colors for CI status icons so that they inherit CI badge colors

** 1.5.0 [2020-09-25 Fri]
Updated styles to latest GitLab =13.4= release.

*** Improved
- Long job names in pipelines are no longer truncated ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/40][!40]])
- GitLab links and transparent buttons should be more consistent
- Added better colors for GitHub-Dark color-scheme

*** Fixed
- Inverted icon color for Gitpod/IDE dropdown menu
- Added new styles for 'usage quota' page in account settings
- Added background color to fallback/broken avatar identicons
- Increased specificity and =fill= color for red/green SVG icons
- Disabled custom Dark-GitLab announcements on user profiles
  - I had it disabled in my configuration menu all this time. :v

** 1.4.4 [2020-09-16 Wed]
Small maintenance update.

*** Fixed
- Some down-arrow icons in the navbar were having different sizes
- There was a conflict with 'add a comment' button in commit diffs
- New button styles were interferring with reply buttons in notes

** 1.4.3 [2020-08-07 Fri]
Added a few more more instances to domain list.

*** Improved
- Added LIGO instances to domain list ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/39][!39]])

*** Fixed
- Added styles for table elements on test reports page

** 1.4.2 [2020-08-03 Mon]
Quick update to fix more colors and horizontal scrollbar in issue boards.

*** Improved
- Added a shim to make new button style look like old 'bordered' buttons
- Added Adélie Linux instance to domain list

*** Fixed
- Resolved white =<th>= elements in org-mode/rST tables
- Added resets for borders on security dashboard page

** 1.4.1 [2020-08-01 Sat]
Quick update to fix more colors and horizontal scrollbar in issue boards.

*** Docs
- Fix a typo in the project readme

*** Fixed
- Added missing colors in MR review mode
- Added colors for various icon colors
- Updated cards on environments page
- Added styles for horizontal scrollbar in issue boards ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/38][!38]], [[https://gitlab.com/vednoc/dark-gitlab/-/issues/132][#132]])

** 1.4.0 [2020-07-30 Thu]
Added integration with IDE color variables, updated info in the readme, and
fixed more styles.

*** Docs
- Updated info in the project readme

*** Improved
- Added a shim for native variables used within the IDE area

*** Fixed
- Text color for branch name in CI tables (Thanks dasJ)
- Image details in project container registry ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/130][#130]])
- Text colors in project container registry ([[ https://gitlab.com/vednoc/dark-gitlab/-/issues/131][#131]])
- Removed a couple of bad rules in Lite version

** 1.3.9 [2020-07-25 Sat]
Small maintenance update.

*** Fixed
- Background color for file tree in diffs ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/129][#129]])
- Refactored border-color overrides
- Background color for blue buttons in issues sidebar

** 1.3.8 [2020-07-01 Wed]
Fix colors for DAG integration.

*** Fixed
- Colors for DAG integration in pipelines ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/128][#128]])

** 1.3.7 [2020-06-23 Tue]
Another small update to fix a few misc things.

*** Fixed
- Link colors on /Container Registry/ page ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/37][!37]])
- Colors and borders for new-ish buttons
- Added missing styles in analytics area
- Colors for date range inputs in setings

** 1.3.6 [2020-06-22 Mon]
Fixing a few things from the =13.1= release.

*** Fixed
- Background for retried pipelines ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/125][#125]])
- Colors for navbar area in alpha dark mode
- Colors for Sourcegraph code search integration
- Colors for un/resolved discussions in Merge Requests

** 1.3.5 [2020-06-17 Wed]
Fixing sticky issue headers.

*** Fixed
- Colors and top offset for sticky issue header

** 1.3.4 [2020-06-16 Tue]
Minor release to fix updated styles for =13.1= release.

*** Fixed
- Colors for updated search bars [[https://gitlab.com/vednoc/dark-gitlab/-/issues/126][#126]]
- Removed styles for retry button in pipelines
- Colors for updated labels

** 1.3.3 [2020-05-21 Thu]
Updating styles for =13.0= release.

*** Documentation
- Improved info in the readme

*** Improved
- More white images in docs/help are now inverted
- Whitespace when performance bar is activated
 
*** Fixed
- Whitespace and focus event for Markdown form on 'edit release' page
- Color for issue weight indicator in boards [[https://gitlab.com/vednoc/dark-gitlab/-/issues/124#][#124]]
- Colors for epics label menu [[https://gitlab.com/vednoc/dark-gitlab/-/issues/123#][#123]]
- Colors in epics related table
- Borders for sign in/register tabs
- Colors for some things in mobile mode
- Colors for 'health status' menu in issues
- Custom code font in diffs
- Colors on project 'packages' page
- Colors for pipelines search filter

** 1.3.2 [2020-05-12 Tue]
Small update to fix a few things.

*** Documentation
- Added a note about modifying the code
- Added day names to dates in the changelog

*** Fixed
- Whitespace for 'newest first' mode in notes
- Colors for CI status icons ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/122][#122]])
- A few new buttons in snippets
- Active pagination in pipelines

** 1.3.1 [2020-05-08 Fri]
Small update to fix a few things.

*** Improved
- Added theme fonts and centered content on GitLab Next page

*** Fixed
- Text color for links in broadcast messages ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/120][#120]])
- Small conflict with table =th= in keyboard shortcuts
- Border colors on issues page in search area

** 1.3.0 [2020-05-07 Thu]
Adding new tweaks and disabling one of the options.

This release disables invert hacks for status icons in Chromium-based browsers.
You can re-enable this option from the configuration menu if you need it. More
info can be found in [[https://gitlab.com/vednoc/dark-gitlab/-/commit/badae69eeec7a4ca9fd20a014e078ffd386ef8f3][badae69e]].

*** Improved
- Colors for status icons no longer require =filter: invert()= hack
- Code areas now have highlights when you hover over the lines

*** Fixed
- Colors on operations dashboard page ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/119][#119]])
- Input group colors in 'new project' area
- Colors for accented links in todos
- Styles for 'add projects' in operations

** 1.2.1 [2020-05-02 Sat]
Small fixes and tweaks for the latest GitLab update.

*** Improved
- Borders and backgrounds for notes in discussions
- Badge colors in design area

*** Fixed
- Missing styles for roadmap page in epics
- A couple of new selectors for image inversion
- Colors for left side of parallel view in diffs
- Double border for 'show unchanged lines' in diffs
- Code blocks in search results area

** 1.2.0 [2020-04-27 Mon]
More polishing and fixing small bugs.

*** Improved
- A bunch of elements inside of 'advanced' area in settings
- Focus state shadow and border colors for inputs
- Colors for code blocks inside of callouts
- Colors for expanded code sections in diffs

*** Fixed
- Broken colors for board-promotion-state ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/113][#113]])
- Another table and price colors on billing page
- A conflict with 'description templates' in MRs
- Secondary button styles and repository buttons
- A bunch of styles for 'integrations' page in settings
- Hardcoded values for broadcast banners
- Bad colors for org-mode table headers
- Colors for code blocks in event lists

** 1.1.1 [2020-04-23 Thu]
Small tweaks and some fixes for the latest GitLab update.

*** Improved
- Author menu in project commits
- Time text color for 'you pushed to...' block

*** Fixed
- Unreadable fast-forward merge status ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/116][#116]])
- Dark-on-dark text for some updated labels
- Initial styles for 'health status' labels
- Faded gradient for dropdown menus

** 1.1.0 [2020-04-22 Wed]
A ton of polish in this update, and some new things as well.

Thanks to everyone that contributed!

*** Added
- More self-hosted instances ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/33][!33]])
- And refined styles for Swagger UI ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/101][#101]])

*** Improved
- A few syntax highlighting tokens
- Added whitespace for 'no contributions'
- Background opacity for issues made today
- CI variables table and sort images ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/107][#107]])
- Border color and background for forms
- Colorized cards in 'project pages' area
- Similar URLs are combined into regex rules
- An empty 'activity block' by adding fake content to it

*** Fixed
- A lot of styles for tables, menus, buttons, alerts
- Default text color for task lists ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/111][#111]])
- Default colors for 'review merge request' mode
- Unicode code now uses symbols ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/34][!34]])
- Active item state for droplab menus ([[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/35][!35]])
- Issue tokens and inputs for linked issues ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/112][#112]])
- Inputs for 'new merge dependencies' ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/112][#112]])
- Blank and promo states for issue boards ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/113][#113]])
- Board scope modal and its item conflicts
- Colors for default callout alerts ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/114][#114]])
- Colors for broadcast notifications ([[https://gitlab.com/vednoc/dark-gitlab/-/issues/115][#115]])
- Hover background for requirements

** 1.0.0 [2020-04-14 Tue]
The rewrite is complete.

This update removes styles for all sub-domains except =next.gitlab.com=, and some
of the custom options. There are too many things to cover, so I'm not going to
do that, but you can go through all 489 commits in [[https://gitlab.com/vednoc/dark-gitlab/-/merge_requests/30][!30]] if you're interested.

Going forward, I'll explore adding some sub-domains/pages back. I rarely use
them to justify putting a lot of effort into making them dark, and DarkReader
will do a decent job anyways.

Finally, I want to take this opportunity to thank everyone for using this
userstyle and helping out with the project. Things wouldn't have been the same
without your help.
